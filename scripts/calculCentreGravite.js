/*
 * Vérifie si la vache est dans le pré 
 */
function VerificationPositionVache() {
    document.getElementById('resultat').innerHTML = "";
    let centreGravite = { x: 0, y: 0 };
    if (CalculsAirCentreGravite(centreGravite)) {
        AppartenancePointPolygone(centreGravite);
    }
    else {
        window.alert("ATTENTION, enclos est défalliant, pas d'espace suffisant pour la vache !");
    }
}


/*
 * Calcule le centre gravité du polygone
 */
function CalculsAirCentreGravite(centreGravite) {
    let aireExistant = false;
    let aire = 0;
    let calculIntermediaire;
    for (let index = 0; index < nombrePiquet; index++) {
        calculIntermediaire = positionPiquets[index].x * positionPiquets[(index + 1) % nombrePiquet].y - positionPiquets[(index + 1) % nombrePiquet].x * positionPiquets[index].y;
        aire = aire + calculIntermediaire;
        centreGravite.x = centreGravite.x + ((positionPiquets[index].x * 1 + positionPiquets[(index + 1) % nombrePiquet].x * 1) * calculIntermediaire);
        centreGravite.y = centreGravite.y + ((positionPiquets[index].y * 1 + positionPiquets[(index + 1) % nombrePiquet].y * 1) * calculIntermediaire);
    }
    if (aire != 0) {
        aire = 0.5 * aire;
        centreGravite.x = (1 / (6 * aire)) * centreGravite.x;
        centreGravite.y = (1 / (6 * aire)) * centreGravite.y;
        aireExistant = true;
        PlacerVache(centreGravite)
    }
    DetailCalcule(Math.abs(aire), centreGravite);
    return aireExistant;
}


/*
 * Permet d'afficher le détail de calcule 
 * avec l'air et le centre de gravité
 */
function DetailCalcule(aire, centreGravite) {
    let sectionDetailCalcule = document.createElement('section');

    let labelAire = GenerationLabel("L'aire de l'enclos : " + aire.toFixed(3))
    sectionDetailCalcule.appendChild(labelAire);

    let labelCentreGravite = GenerationLabel("Centre de gravité du pré : X : " + centreGravite.x.toFixed(3) + " et Y : " + centreGravite.y.toFixed(3))
    sectionDetailCalcule.appendChild(labelCentreGravite);

    document.getElementById('resultat').appendChild(sectionDetailCalcule);
}


/*
 * Calcul si le centre de graviter appartient au polygone
 */
function AppartenancePointPolygone(centreGravite) {
    let somme = 0.0;
    let thetai;

    for (let index = 0; index < nombrePiquet; index++) {
        thetai = CalculThetai(centreGravite, index);
        somme = somme + thetai;
    }

    if (somme.toFixed(13) != 0) { //La donnée est arrondie ("toFixed()") afin de palier au probleme de precision de JavaScript
        label = GenerationLabel("Relax ! La vache est dans le pré.")
        document.getElementById('resultat').appendChild( label );
    }
    else {
        label = GenerationLabel("ATTENTION, la vache est hors du pré !!!")
        document.getElementById('resultat').appendChild( label );
        window.alert("ATTENTION, la vache est hors du pré !!!");
    }
}


/*
 * Calcul de l'angle thetai à l'index i
 */
function CalculThetai(centreGravite, index) {
    let vecteursGS = [{ x: 0, y: 0 }, { x: 0, y: 0 }];
    vecteursGS[0].x = positionPiquets[index].x - centreGravite.x;
    vecteursGS[0].y = positionPiquets[index].y - centreGravite.y;
    vecteursGS[1].x = positionPiquets[(index + 1) % nombrePiquet].x - centreGravite.x;
    vecteursGS[1].y = positionPiquets[(index + 1) % nombrePiquet].y - centreGravite.y;

    let normeVecteursGS = { x: 0, y: 0 };
    normeVecteursGS.x = Math.sqrt(Math.pow(vecteursGS[0].x, 2) + Math.pow(vecteursGS[0].y, 2));
    normeVecteursGS.y = Math.sqrt(Math.pow(vecteursGS[1].x, 2) + Math.pow(vecteursGS[1].y, 2));
    multiplicationVecteur = (vecteursGS[0].x * vecteursGS[1].x) + (vecteursGS[0].y * vecteursGS[1].y);

    result = Math.acos(multiplicationVecteur / (normeVecteursGS.x * normeVecteursGS.y)); //Attention a la division par 0 et pas initialisé

    let determinant = (vecteursGS[0].x * vecteursGS[1].y) - (vecteursGS[0].y * vecteursGS[1].x);
    if (determinant < 0) {
        result = result * (-1);
    }
    return result;
}


/*
 * Génération de label 
 */
function GenerationLabel(texte) {
    let label = document.createElement('label');
    let textLabel = document.createTextNode(texte);
    label.appendChild(textLabel);

    return label;
}