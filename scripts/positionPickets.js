var positionPiquets = [];
var nombrePiquet = 0;
var inputNombrePiquet = document.getElementById('nombrePiquet');
inputNombrePiquet.oninput = changementNombrePiquet;


/*
 * On accède à cette fonction si l'utilisateur commence à saisir
 * dans le label correspondant au nombre de piquets
 */
function changementNombrePiquet() {
    let nombrePiquetAObtenir = inputNombrePiquet.value;
    document.getElementById('resultat').innerHTML = "";

    if (nombrePiquetAObtenir == "") {
        nombrePiquetAObtenir = 0;
    }

    ControleSaisieNombrePiquet(nombrePiquetAObtenir);
}


/*
 * Controle de saisi pour obtenir un nombre supérieur ou égale à 0.
 * Sinon reafectation de l'ancienne valeur saisie.
 */
function ControleSaisieNombrePiquet(nombrePiquetAObtenir){
    if (/^\d*$/.test(nombrePiquetAObtenir)) {
        nombrePiquetAObtenir = LimitationPiquet(nombrePiquetAObtenir);
        if (nombrePiquetAObtenir > nombrePiquet) { //L'utilisateur souhaite ajouter des piquets
            for (nombrePiquet; nombrePiquet < nombrePiquetAObtenir; nombrePiquet++) {
                CreationPiquet();
                PiquetExiste();
                AjouterPiquet3D(nombrePiquet, positionPiquets[nombrePiquet]); //Ajouter un piquet au plan 3D
            }
        }
        else if (nombrePiquetAObtenir < nombrePiquet) { //L'utilisateur souhaite enlever des piquets
            SuppressionAffichagePiquet(nombrePiquetAObtenir);
        }
    }
    else { 
        inputNombrePiquet.value = nombrePiquet;
    }
}


/*
 * Test si le nombre de piquet est inférieur ou égale 50.
 * Envoie un message si celui ci dépasse les 50 piquets.
 * 
 * Le paramètre :
 * nombrePiquetAObtenir -> stock la valeur saisie par l'utilisateur et la limite à 50.
 */
function LimitationPiquet(nombrePiquetAObtenir) {
    nombrePiquetAObtenir = parseInt(nombrePiquetAObtenir, 10)
    if (nombrePiquetAObtenir > 50) {
        nombrePiquetAObtenir = 50;
        inputNombrePiquet.value = nombrePiquetAObtenir;
        window.alert("Attention, votre voisin a visiblement ajouté des piquets !")
    }
    return nombrePiquetAObtenir;
}


/*
 * Creation des piquets avec :
 * une section
 * un label avec son nombre (allant de 1 à 50)
 * un input position X 
 * un input position Y
 * 
 * Et l'affichage de la section dans l'html
 */
function CreationPiquet() {
    let sectionPiquet = document.createElement('section');
    sectionPiquet.id = 'piquet' + nombrePiquet;

    let labelPiquet = document.createElement('label');
    let textLabelPiquet = document.createTextNode('Piquet n°' + (nombrePiquet + 1) + ' :');
    labelPiquet.appendChild(textLabelPiquet);
    sectionPiquet.appendChild(labelPiquet);

    let inputXPiquet = document.createElement('input');
    inputXPiquet.id = 'piquet' + nombrePiquet + 'Positionx';
    inputXPiquet.placeholder = 'X';
    inputXPiquet.setAttribute('oninput', 'changementPositionPiquet(' + nombrePiquet + ', "x")')
    sectionPiquet.appendChild(inputXPiquet);

    let inputYPiquet = document.createElement('input');
    inputYPiquet.id = 'piquet' + nombrePiquet + 'Positiony';
    inputYPiquet.placeholder = "Y";
    inputYPiquet.setAttribute('oninput', 'changementPositionPiquet(' + nombrePiquet + ', "y")')
    sectionPiquet.appendChild(inputYPiquet);

    document.getElementById('positionPiquet').appendChild(sectionPiquet);
}


/*
 * Si le piquet existe on recupère les valeurs dans le tableau.
 * Sinon, si le piquet n'existait pas, alors on met 0 à X et à Y
 */
function PiquetExiste() {
    if (positionPiquets[nombrePiquet]) {
        let positionXPiquet = positionPiquets[nombrePiquet].x;
        let positionYPiquet = positionPiquets[nombrePiquet].y;

        if (positionXPiquet != 0 && positionXPiquet != undefined) {
            document.getElementById("piquet" + nombrePiquet + "Positionx").value = positionXPiquet;
        }
        else {
            positionPiquets[nombrePiquet].x = 0;
        }
        if (positionYPiquet != 0 && positionYPiquet != undefined) {
            document.getElementById("piquet" + nombrePiquet + "Positiony").value = positionYPiquet;
        }
        else {
            positionPiquets[nombrePiquet].y = 0;
        }
    }
    else {
        positionPiquets[nombrePiquet] = {x:0, y:0};
    }
}


/*
 * Supression du piquet sur l'affichage 
 * parametre : nombrePiquetAObtenir 
 * Permet de stocker le nombre de piquet voulu par l'utisateur
 */
function SuppressionAffichagePiquet(nombrePiquetAObtenir){
    for (nombrePiquet; nombrePiquet > nombrePiquetAObtenir; nombrePiquet--) {
        document.getElementById('positionPiquet').removeChild(document.getElementById('piquet' + (nombrePiquet - 1)));
        SupprimerPiquet(nombrePiquet - 1)
    }
}


/*
 * L'utilisateur saisie la position des piquets dans des "input", qu'on récupère pour les stocker dans un tableau.
 *
 * numeroPiquet -> est l'index du piquet dans le tableau sur lequel on modifie la valeur.
 * Si reperePiquet est égale à 0, alors c'est qu'on modifie la valeur de X du piquet, sinon c'est la valeur de Y du piquet
 */
function changementPositionPiquet(numeroPiquet, reperePiquet) {
    
    document.getElementById('resultat').innerHTML = "";
    let valeurPositionPiquetSaisi = document.getElementById("piquet" + numeroPiquet + "Position" + reperePiquet).value;

    if (valeurPositionPiquetSaisi == "") {
        valeurPositionPiquetSaisi = 0;
    }

    //Test regex :
    //test si c'est un nombre décimale aussi bien positif que négatif
    if (/^-?\d*[.,]?\d*$/.test(valeurPositionPiquetSaisi)) {
        positionPiquets[numeroPiquet][reperePiquet] = valeurPositionPiquetSaisi;
    }
    else {
        document.getElementById("piquet" + numeroPiquet + "Position" + reperePiquet).value = positionPiquets[numeroPiquet][reperePiquet];
    }
    if (!isNaN(valeurPositionPiquetSaisi)) {
        EnleverVache();
        BougerPiquet(positionPiquets[numeroPiquet], numeroPiquet);
    }
}