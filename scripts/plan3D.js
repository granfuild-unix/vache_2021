var container;
var camera, scene, renderer, fil;
var GPiquet, MPiquet, MFil;
var piquets = [];
var coefDistance = 1;
var vacheErreur = false;
var vache = [];
vache.presenceScene = false;


Init();
Animer();

/*================================================== Fonctions appelé lors de l'initialisation ==================================================*/

function Afficher() {
    renderer.render(scene, camera);
}

function Animer() {
    requestAnimationFrame(Animer);
    Afficher();
}

function onWindowResize() {
    camera.aspect = container.offsetWidth / container.offsetWidth;
    camera.updateProjectionMatrix();
    renderer.setSize(container.offsetWidth, container.offsetWidth);
}

function Init() {
    container = document.getElementById("Plan3D");
    scene = new THREE.Scene();

    InitialisationLumieres();
    InitialisationCamera();

    ChargementVache();
    InitialisationPre();
    InitialisationPiquet();
    InitialisationMaterielFil();

    renderer = new THREE.WebGLRenderer({ alpha: true });
    renderer.gammaOutput = true;
    renderer.gammaFactor = 2.2;
    renderer.setSize(container.offsetWidth, container.offsetWidth);
    container.appendChild(renderer.domElement);
    window.addEventListener('resize', onWindowResize, false);
}


function InitialisationLumieres() {
    var ambient = new THREE.AmbientLight(0x222222);
    scene.add(ambient);

    var light = new THREE.PointLight();
    light.position.set(0, -10, 10);
    scene.add(light);
}

function InitialisationCamera() {
    camera = new THREE.PerspectiveCamera(90, container.offsetWidth / container.offsetWidth, 0.01, 2000);
    camera.position.x = 0;
    camera.position.y = -8.1;
    camera.position.z = 5;

    camera.rotation.x = 1;
    scene.add(camera);
}

/*
 * On charge la vache dans la mémoire avec les paramètres d'initialisation
 */
function ChargementVache() {
    const loader = new THREE.GLTFLoader();

    loader.load('./model3D/cow.glb', function (gltf) {
        vache = gltf.scene;
        vache.rotation.x = Math.PI / 2;
        vache.rotation.y = (Math.PI / 4) * 3;
        vache.position.z += 0.50;
        vache.scale.set(0.75, 0.75, 0.75)
    },
        undefined, function (error) {
            vacheErreur = true
            throw 'Impossible de charger la vache';
        });
}

function InitialisationPre() {
    var GPre = new THREE.BoxGeometry(10, 10, 1)
    var MPre = new THREE.MeshPhongMaterial({
        color: 0x00ff00,
        specular: 0x555555,
        shininess: 50
    });
    var Pre = new THREE.Mesh(GPre, MPre);
    scene.add(Pre);
}

function InitialisationPiquet() {
    GPiquet = new THREE.BoxGeometry(0.1, 0.1, 1)
    MPiquet = new THREE.MeshPhongMaterial({
        color: 0xf4a460,
        specular: 0x555555,
        shininess: 50
    });
}

function InitialisationMaterielFil() {
    MFil = new THREE.LineBasicMaterial({
        color: 0xdddddd
    });
}


/*================================================== Fonctions appelées apres initialisation ==================================================*/

/*
 * Ajoute le piquet sur le plans 3D.
 */
function AjouterPiquet3D(numeroPiquet, positionsPiquet) {
    piquets[numeroPiquet] = new THREE.Mesh(GPiquet, MPiquet);

    piquets[numeroPiquet].position.z = 1;
    scene.add(piquets[numeroPiquet]);
    BougerPiquet(positionsPiquet, numeroPiquet);
}


/*
 * Change l'emplacement du piquet en X et Y
 * et gere la fonction DeZoom
 */
function BougerPiquet(positionsPiquet, numeroPiquet) {

    piquets[numeroPiquet].position.x = (positionsPiquet.x / coefDistance);
    if (positionsPiquet.x / coefDistance > 5 || positionsPiquet.x / coefDistance < -5) {
        DeZoom(positionsPiquet.x);
    }

    piquets[numeroPiquet].position.y = (positionsPiquet.y / coefDistance);
    if (positionsPiquet.y / coefDistance > 5 || positionsPiquet.y / coefDistance < -5) {
        DeZoom(positionsPiquet.y);
    }

    GestionZoom()
    ChangerFil()
}


/*
 * Supprime un piquet du plans 3D
 */
function SupprimerPiquet(numeroPiquet) {
    scene.remove(piquets[numeroPiquet]);
    piquets.splice(numeroPiquet, 1)
    GestionZoom()
    ChangerFil()
}


/*
 * Permet de savoir si on a besoins de zoom :
 * si la plus grande valeur est inferieur 5 
 * le zoom est limité a un coeficient de 1 (1x/y = 1 metre)
 */
function GestionZoom() {
    let plusGrandeValeur = 0;
    for (let piquet = 0; piquet < piquets.length; piquet++) {
        if (piquets[piquet].position.x > plusGrandeValeur) {
            plusGrandeValeur = piquets[piquet].position.x;
        }
        if (piquets[piquet].position.y > plusGrandeValeur) {
            plusGrandeValeur = piquets[piquet].position.y;
        }
    }

    if (plusGrandeValeur < 5 && coefDistance > 1) {
        Zoom(plusGrandeValeur)
    }
}


/*
 * Zoom : 
 * Determine la valeur d'un nouveau coeficient 
 * (1metre = ...x )
 */
function Zoom(plusGrandeValeur) {
    let coefDifference = plusGrandeValeur / 5;
    if (coefDistance * coefDifference < 1) {
        coefDifference = 1 / coefDistance;
        coefDistance = 1;
    }
    else {
        coefDistance = coefDistance * coefDifference;
    }
    ChangeCoefDistance(coefDifference)
}


/*
 * DeZoom : 
 * Determine la valeur d'un nouveau coeficient 
 * (1 mètre = ...x )
 */
function DeZoom(position) {
    let coefDifference = (position / 5) / coefDistance;
    coefDistance = (position / 5);
    ChangeCoefDistance(coefDifference)
}


/*
 * On change la position des piquets si celui doit etre zommer ou dezoomer
 */
function ChangeCoefDistance(coefDifference) {
    for (let piquet = 0; piquet < piquets.length; piquet++) {
        piquets[piquet].position.x = piquets[piquet].position.x / coefDifference;
        piquets[piquet].position.y = piquets[piquet].position.y / coefDifference;
    }
}



/*
 * Lorsque l'on change les coordonnées d'un piquet, on supprime le fil precedent et on ajoute un nouveau.
 */
function ChangerFil() {
    EnleverVache();
    scene.remove(fil)
    let points = [];

    for (let piquet = 0; piquet < piquets.length; piquet++) {
        points.push(new THREE.Vector3(piquets[piquet].position.x, piquets[piquet].position.y, 1.25));
    }

    if (piquets[0]) {
        points.push(new THREE.Vector3(piquets[0].position.x, piquets[0].position.y, 1.25));
    }

    GFil = new THREE.BufferGeometry().setFromPoints(points);
    fil = new THREE.Line(GFil, MFil);
    scene.add(fil);
}



/*
 * Lorsque l'on calcule l'air du pré, on place la vache sur le centre de gravité de ce dernier dans le plans 3D.
 */
function PlacerVache(positions) {
    if (vacheErreur == false) {
        vache.position.x = positions.x / coefDistance;
        vache.position.y = positions.y / coefDistance;
        scene.add(vache);
        vache.presenceScene = true;
    }
}


/*
 * Si la vache est sur le plans 3D et qu'on change la position d'un piquet,
 * alors on enleve la vache du plans 3D.
 */
function EnleverVache() {
    if (vacheErreur == false && vache.presenceScene == true) {
        scene.remove(vache);
        vache.presenceScene = false;
    }
}